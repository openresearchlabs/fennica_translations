Data
====

Let us load the data.

Analysis
========

Non-empty entries in each field (%):

<table>
<thead>
<tr class="header">
<th></th>
<th style="text-align: right;">x</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>245a</td>
<td style="text-align: right;">100.0</td>
</tr>
<tr class="even">
<td>041h</td>
<td style="text-align: right;">2.8</td>
</tr>
<tr class="odd">
<td>041k</td>
<td style="text-align: right;">0.0</td>
</tr>
<tr class="even">
<td>500a</td>
<td style="text-align: right;">93.6</td>
</tr>
<tr class="odd">
<td>546a</td>
<td style="text-align: right;">7.6</td>
</tr>
</tbody>
</table>

# Translation analyses for Fennica

## Output

The scripts in this folder generate the [translations.md](translations.md) file.

## Replicate the analyses

Run the [main.R](main.R) script to convert the Rmd document into
readable format.

## Input data

The Fennica CSV file has been parsed from the original XML files
provided by the National Library of Finland by using the
COMHIS/MARCdata parser. More details will be added.

## Contact

Leo Lahti


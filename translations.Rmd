---
title: "Fennica analyses"
author: "Leo Lahti & Laura Ivaska"
date: "`r Sys.Date()`"
output: 
  beamer_presentation:
    theme: "boxes"
    colortheme: "orchid"
    fonttheme: "professionalfonts"
    dev: "pdf"
fontsize: 12pt
---

# Data

Let us load the data.

```{r data, echo=FALSE, message=FALSE, warning=FALSE, cache=FALSE}

```

# Analysis

Non-empty entries in each field (%):


```{r analysis, echo=FALSE, message=FALSE, warning=FALSE, cache=FALSE}
library(knitr)
kable(round(100 * colMeans(!is.na(df)), 1))
```


